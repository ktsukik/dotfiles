#!/bin/sh
## launch dmenu

dmenu_run  -f -i -l 4 -p ">" \
	-nb "#283e58" \
        -sb "#f84d70" \
        -nf "#c5c4c5" \
        -sf "#181619" \
        -x 600 -y 250 \
        -h 30 -w 400 \

exit 0
