#!/usr/bin/env python
# load.py
import subprocess
import os

# config
workspace = 1
dashboard_layout = os.path.expanduser('~/.config/i3/dashboard-alt.json')
usage_layout = os.path.expanduser('~/.config/i3/usage.json')

dashboard_cmds = [
    {'name': 'ranger', 'cmd': 'ranger', 'opts': ' ', 'shell': 'True'},
    {'name': 'neofetch', 'cmd': 'neofetch | lolcat', 'opts': ' ', 'shell': 'True'},
    {'name': 'tty-clock', 'cmd': 'tty-clock', 'opts': '-c -C 5 -B -f \'%a, %b %d %Y %Z\'', 'shell': 'True'},
    {'name': 'alsamixer', 'cmd': 'alsamixer', 'opts': '-c 0', 'shell': 'True'},
    {'name': 'ytop', 'cmd': 'ytop', 'opts': ' ', 'shell': 'True'}
]

dashboard_alt_cmds = [
    {'name': 'ranger', 'cmd': 'ranger', 'opts': ' ', 'shell': 'True'},
    {'name': 'neofetch', 'cmd': 'neofetch', 'opts': ' ', 'shell': 'True'},
    {'name': 'alsamixer', 'cmd': 'alsamixer', 'opts': '-c 0', 'shell': 'True'},
    {'name': 'tty-clock', 'cmd': 'tty-clock', 'opts': '-c -C 5 -B -f \'%a, %b %d %Y %Z\'', 'shell': 'True'},
    {'name': 'cava', 'cmd': 'cava', 'opts': ' ', 'shell': 'True'},
    {'name': 'mplayer', 'cmd': 'mplayer', 'opts': ' ', 'shell': 'True'},
    {'name': 'cmus', 'cmd': 'cmus', 'opts': ' ', 'shell': 'True'},
    {'name': 'ytop', 'cmd': 'ytop', 'opts': ' ', 'shell': 'True'}
]

usage_cmds = [
    {'name': 'pavucontrol', 'cmd': 'pavucontrol', 'opts': ' ', 'shell': 'False'},
    {'name': 'urxvt', 'cmd': 'bash', 'opts': ' ', 'shell': 'True'},
    {'name': 'urxvt', 'cmd': 'bash', 'opts': ' ', 'shell': 'True'},
    {'name': 'keepassxc', 'cmd': 'keepassxc', 'opts': ' ', 'shell': 'False'}
]

def run_commands(cmds):
    runstrings = []
    for cmd in cmds:
        name = cmd['name']
        binary = cmd['cmd']
        opts = cmd['opts']
        shell_flag = cmd['shell']
        string = ''
        if shell_flag == 'True':
            string = 'urxvt -name {} -e $SHELL -c "{} {}; $SHELL" && $SHELL &'.format(name, binary, opts)
            subprocess.run(string, shell=shell_flag)
        else:
            string = '{} {}'.format(binary, opts)
            os.spawnlp(os.P_NOWAIT, binary, name, opts)

#i3cmd_usage = 'i3-msg "workspace {}; append_layout{}"'.format(3, usage_layout)

#subprocess.run(i3cmd_usage, shell=True)
#run_commands(usage_cmds)

i3cmd_dashboard = 'i3-msg "workspace {}; append_layout {}"'.format(workspace, dashboard_layout)

subprocess.run(i3cmd_dashboard, shell=True)
run_commands(dashboard_alt_cmds)

# switch back to dhasboard
#subprocess.run('i3-msg "workspace {};"'.format(workspace))
