# Path to your oh-my-zsh installation.
export ZSH="/home/iamo/.oh-my-zsh"

# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
# set zsh theme
# check if we have 256 colors
if [ $(tput colors) != "256" ]; then
    # set non-256 color theme that is tty friendly
    ZSH_THEME="gallifrey"
else
    # set 256 color theme
    # powerlevel10k theme https://github.com/romkatv/powerlevel10k
    ZSH_THEME="powerlevel10k/powerlevel10k"

    # Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
    # Initialization code that may require console input (password prompts, [y/n]
    # confirmations, etc.) must go above this block, everything else may go below.
    if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
      source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
    fi
fi

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
HIST_STAMPS="yyyy.mm.dd"

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git tmux rsync vscode nanoc nmap colorize
         zsh-syntax-highlighting zsh-completions
         zsh-history-substring-search zsh-autosuggestions)

# source oh-my-zsh
source $ZSH/oh-my-zsh.sh

##############
# completion #
##############

# cast insensitive tab completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
# Colored completion (differnt colors for dirs/files/etc
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
# automatically find new executables in path
zstyle ':completion:*' rehash true
# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache
# completer settings
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
#zstyle ':completion:*' list-colors ''
#zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=** r:|=**'
zstyle :compinstall filename '/home/iamo/.zshrc'

###########
# history #
###########

HISTFILE=~/.zhistory
HISTSIZE=1500
SAVEHIST=500
# Don't consider certain char acters part of the word
WORDCHARS=${WORDCHARS//\/[&.;]}
setopt appendhistory autocd extendedglob notify
bindkey -e

##################
# zsh overrides  #
##################

# shortcut to exit shell on partial command line
# have <CTRL+d> close shell if command line is filled
function exit_zsh() { exit }
zle -N exit_zsh
bindkey '^D' exit_zsh

#######################
# fish style features #
#######################

# Use syntax highlighting
#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# Use history substring search
#source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
# bind UP and DOWN arrow keys to history substring search
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

##################
# color manpages #
##################

export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[01;32m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;47;34m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'
export LESS=-r

###############
# theming zsh #
###############

autoload -Uz compinit colors zcalc
compinit -d
colors

###########
# exports #
###########

export EDITOR="/usr/bin/nano"
export VISUAL="/usr/bin/nano"
export GIT_PAGER=""
export PRE_CXX="ccache"
export MANPATH="/usr/share/man:/usr/local/share/man"

##########
# alias' #
##########

#alias emacs="emacs -nw"
alias nano="nano -l"
alias startx="startx -- vt1 &> .xorg-log"
alias tty-clock="tty-clock -c -C 5 -B -f '%a, %b %d %Y %Z'"
alias clip="xclip -selection clip"
alias gitclog="git log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias ls="ls --color=always"
alias cp="cp -i"

# tldr
function tldr() {
    curl "cheat.sh/$1"
}
alias tldr="tldr $1"

# weather
function weather() {
    curl "wttr.in/$1"
}
alias weather="weather $1"

########
# path #
########

# scripts
PATH="$PATH:$HOME/.local/scripts"

# python
PATH="$PATH:$HOME/.local/bin"

# golang
GOPATH="$HOME/.go"
PATH="$PATH:$HOME/.go/bin"

# rubygem
PATH="$PATH:$HOME/.gem/ruby/2.7.0/bin"

# cargo
PATH="$PATH:$HOME/.cargo/bin"

# npm
NPM_PACKAGES="$HOME/.npm-packages"
PATH="$PATH:$NPM_PACKAGES/bin:/usr/games/bin:$HOME/.local/lib"

####################
# restore last cwd #
####################

# save path on cd
function cd {
    builtin cd $@
    pwd > ~/.last_dir
}

# restore last path
[[ -f ~/.last_dir ]] && cd `cat ~/.last_dir`

# clear cwd on exit shell
function onExit() {
    [[ -f ~/.last_dir ]] && rm ~/.last_dir
}
trap onExit EXIT

#########
# pywal #
#########

(cat ~/.cache/wal/sequences &)
cat ~/.cache/wal/sequences
source ~/.cache/wal/colors-tty.sh

##########
# sdkman #
##########

export SDKMAN_DIR="/home/iamo/.sdkman"
[[ -s "/home/iamo/.sdkman/bin/sdkman-init.sh" ]] && source "/home/iamo/.sdkman/bin/sdkman-init.sh"

######################
# init powerlevel10k #
######################

[[ -f ~/.p10k.zsh ]] && source ~/.p10k.zsh
[[ -f ~/.p10k-config.zsh ]] && source ~/.p10k-config.zsh
