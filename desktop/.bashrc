# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !

# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Put your fun stuff here.

##########
# alias' #
##########

alias emacs="emacs -nw"
alias nano="nano -w"
alias startx="startx -- vt1"
alias tty-clock="tty-clock -c -C 5 -B -f '%a, %b %d %Y %Z'"
alias clip="xclip -selection clip"
alias gitclog="git log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias ls="ls --color=always"

#############
# functions #
#############

# common linux command examples
function tldr() {
    curl cheat.sh/$1
}

###########
# exports #
###########

export PATH=$PATH:~/.local/scripts:~/.local/bin:~/.go/bin:~/.cargo/bin

#########
# pywal #
#########

(cat ~/.cache/wal/sequences &)
cat ~/.cache/wal/sequences
source ~/.cache/wal/colors-tty.sh

######
# go #
######

GOPATH=~/.go

########
# ruby #
########

PATH=$PATH:~/.gem/ruby/2.7.0/bin

#######
# npm #
#######
NPM_PACKAGES=~/.npm-packages
PATH=$NPM_PACKAGES/bin:$PATH:/usr/games/bin:$HOME/.local/lib

####################
# restore last cwd #
####################

# save path on cd
function cd {
    builtin cd $@
    pwd > ~/.last_dir
}

# restore last saved path
if [ -f ~/.last_dir ]
    then cd `cat ~/.last_dir`
fi

# if we exit the shell, clear last cwd
function onExit() {
    if [ -f ~/.last_dir ] ; then
        rm ~/.last_dir
    fi
}
trap onExit EXIT

##########
# sdkman #
##########

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/tsuki/.sdkman"
[[ -s "/home/tsuki/.sdkman/bin/sdkman-init.sh" ]] && source "/home/tsuki/.sdkman/bin/sdkman-init.sh"
