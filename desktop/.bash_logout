# /etc/skel/.bash_logout

# This file is sourced when a login shell terminates.

# Clear the screen for security's sake.
clear

# remove last dir
if [ -f ~/.last_dir ] 
    then rm ~/.last_dir
fi
