#!/bin/sh

killall -q compton
while pgrep -u $UID -x compton >/dev/null; do sleep 1; done

compton --config $HOME/.config/compton/config -b

echo "compton was launched"
