#!/bin/sh

# rofi launcher
rofi -combi-modi window,drun,run,ssh -show combi -modi combi -lines 4 -width 30 -padding 70 -cache-dir ~/.rofi

exit 0
