;; inet.el

;;----------
;; packages 
;;----------
(require 'package)

;; add packages sources
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;; package init
(package-initialize)

;; add packages to list
(add-to-list 'package-archives '(use-package . "melpa") t)

;; install packages
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(use-package auto-package-update
  :ensure t
  :config
  (setq auto-package-update-delete-old-versions t
        auto-package-update-interval 4)
  (auto-package-update-maybe))
(use-package markdown-mode :ensure t)
(use-package python-mode :ensure t)
(use-package elixir-mode :ensure t)
(use-package js2-mode :ensure t)
(use-package neotree :ensure t)
(use-package color-theme :ensure t)
;(use-package molokai-theme
;  :ensure t
;  :init (load-theme 'molokai t))
(use-package jedi 
   :ensure t
   :init 
   (add-hook 'python-mode-hook 'jedi:setup)
   (add-hook 'python-mode-hook 'jedi:install-server)
   :config
   (setq jedi:complete-on-dot t))

;; configure neotree
(global-set-key [f8] 'neotree-toggle)

;;----------------
;; config options
;;----------------
(setq backup-by-copying t
      backup-directory-alist '(("." . "~/.emacs-saves/"))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)
(setq auto-save-file-name-transforms
      `((".*" "~/.emacs-saves/" t)))

(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq tab-width 4)
(global-display-line-numbers-mode)
(setq linum-format "%4d /u2502 ")

(add-to-list 'auto-mode-alist '("\\.njk\\'" . html-mode))

;;------------------
;; custom variables
;;------------------

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages (quote (neotree elixir-mode pkg-info diminish bind-key))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
