# p10k-config.zsh

# os_icon
typeset -g POWERLEVEL9K_OS_ICON_FOREGROUND=0
typeset -g POWERLEVEL9K_OS_ICON_BACKGROUND=7

# cwd
typeset -g POWERLEVEL9K_SHORTEN_STRATEGY=truncate_to_first_and_last
typeset -g POWERLEVEL9K_SHORTEN_DELIMITER=".."

# reload p10k
p10k reload
