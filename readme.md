# dotfiles

. files

## current setup

* desktop
   - todo: take screenshot
* [laptop](/laptop)
   - ![laptop dashboard](/extra/images/laptop-dashboard-screenshot.jpg)
       - setup: i3-gaps, urxvt, zsh, oh-my-zsh, powerlevel9k external zsh theme, pywal
       - apps: ranger, tty-clock, cava, neofetch, alsamixer, ytop, mplayer, cmus

## [changelog](/changelog.md)

## [license](/license)
